package name.uhmann.visual_time_tracker.ui.statistics

data class StatisticsItem(
    val name: String,
    val minutes: Int,
    val color: Int
)
