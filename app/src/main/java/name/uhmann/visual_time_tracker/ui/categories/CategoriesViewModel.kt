package name.uhmann.visual_time_tracker.ui.categories

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import name.uhmann.visual_time_tracker.data.AppDatabase
import name.uhmann.visual_time_tracker.data.AppRepository
import name.uhmann.visual_time_tracker.data.Category
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoriesViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = AppRepository(AppDatabase.getDatabase(application).dao())

    val selectedCategory = MutableLiveData<Category?>()

    //
    // Categories
    //

    fun insertCategory(category: Category) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertCategory(category)
    }

    fun selectCategories() = repository.selectCategories()

    fun updateCategory(category: Category) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateCategory(category)
    }

    fun deleteCategory(categoryId: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteCategory(categoryId)
    }
}