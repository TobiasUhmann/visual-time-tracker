package name.uhmann.visual_time_tracker.ui.statistics

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.ui.statistics.StatisticsListAdapter.StatisticsViewHolder
import kotlinx.android.synthetic.main.statistics_item.view.*

class StatisticsListAdapter(private val context: Context) : Adapter<StatisticsViewHolder>() {

    private var items = emptyList<ListItem>()

    data class ListItem(
        val color: Int,
        val name: String,
        val hours: Int,
        val minutes: Int,
        val percent: Int
    )

    inner class StatisticsViewHolder(itemView: View) : ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatisticsViewHolder {
        val inflater = LayoutInflater.from(context)
        val itemView = inflater.inflate(R.layout.statistics_item, parent, false)

        return StatisticsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: StatisticsViewHolder, position: Int) {
        val item = items[position]

        with(holder.itemView) {
            statisticsItem_colorView.setBackgroundColor(item.color)
            statisticsItem_nameText.text = item.name
            statisticsItem_timeText.text = "${item.hours}:%02d".format(item.minutes)
            statisticsItem_percentText.text = "${item.percent}%"
        }
    }

    override fun getItemCount() = items.size

    internal fun setItems(items: List<ListItem>) {
        this.items = items
        notifyDataSetChanged()
    }
}