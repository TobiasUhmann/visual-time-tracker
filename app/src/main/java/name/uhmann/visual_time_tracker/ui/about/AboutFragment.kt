package name.uhmann.visual_time_tracker.ui.about

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.about_fragment.view.*
import name.uhmann.visual_time_tracker.BuildConfig
import name.uhmann.visual_time_tracker.R

class AboutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.about_fragment, container, false)

        val appVersion = BuildConfig.VERSION_NAME
        view.about_info.text = resources.getString(R.string.about_info_text, appVersion)

        return view
    }
}
