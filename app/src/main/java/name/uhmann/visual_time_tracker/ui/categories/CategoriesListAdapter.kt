package name.uhmann.visual_time_tracker.ui.categories

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.data.Category
import kotlinx.android.synthetic.main.category_item.view.*

const val VIEW_TYPE_CATEGORY_ITEM = 1
const val VIEW_TYPE_HEADER = 2
const val VIEW_TYPE_FOOTER = 3

/**
 * Manages list consisting of categories + heading + archived categories + footer.
 * The (empty) footer's purpose is to allow scrolling beyond the FAB.
 */
class CategoriesListAdapter(
    private val context: Context,
    private val onItemClickListener: (Category) -> Any,
    private val onNameClickListener: (Category) -> Any,
    private val onColorClickListener: (Category) -> Any,
    private val onShortTextClickListener: (Category) -> Any
) : Adapter<ViewHolder>() {

    private var activeCategories = emptyList<Category>()
    private var archivedCategories = emptyList<Category>()

    private var selectedCategory: Category? = null

    //
    // Inner classes
    //

    inner class CategoryViewHolder(itemView: View) : ViewHolder(itemView) {
        private val name = itemView.categoryItem_nameTextView
        private val color = itemView.categoryItem_color
        private val shortText = itemView.categoryItem_shortText
        private val border = itemView.categoryItem_border

        fun bind(category: Category) {
            name.text = category.name

            color.setBackgroundColor(category.color)

            shortText.text = category.shortText
            shortText.setBackgroundColor(category.color)

            val selectedCategory = selectedCategory
            val isSelected = selectedCategory != null && selectedCategory.id == category.id
            border.visibility = if (isSelected) VISIBLE else GONE

            itemView.setOnClickListener { onItemClickListener(category) }
            name.setOnClickListener { onNameClickListener(category) }
            color.setOnClickListener { onColorClickListener(category) }
            shortText.setOnClickListener { onShortTextClickListener(category) }
        }
    }

    class TitleViewHolder(itemView: View) : ViewHolder(itemView)
    class FooterViewHolder(itemView: View) : ViewHolder(itemView)

    //
    // Overridden methods
    //

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)

        return when (viewType) {
            VIEW_TYPE_CATEGORY_ITEM -> {
                val view = inflater.inflate(R.layout.category_item, parent, false)
                CategoryViewHolder(view)
            }

            VIEW_TYPE_HEADER -> {
                val view = inflater.inflate(R.layout.category_header_item, parent, false)
                TitleViewHolder(view)
            }

            VIEW_TYPE_FOOTER -> {
                val view = inflater.inflate(R.layout.footer_item, parent, false)
                FooterViewHolder(view)
            }

            else -> throw AssertionError("Should not happen. Invalid view type: $viewType")
        }
    }

    /**
     * Category at position -> Get category from active/archived categories,
     *                         respectively, and bind it
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_CATEGORY_ITEM) {
            val category = when {
                isActiveCategoryPosition(position) -> activeCategories[position]
                isArchivedCategoryPosition(position) -> archivedCategories[position - activeCategories.size - 1]

                else -> throw AssertionError("Should not happen. Invalid position: $position")
            }

            (holder as CategoryViewHolder).bind(category)
        }
    }

    /**
     * Only active categories   -> #active + footer
     * Also archived categories -> #active + header + #archived + footer
     */
    override fun getItemCount() =
        if (archivedCategories.isEmpty())
            activeCategories.size + 1
        else
            activeCategories.size + 1 + archivedCategories.size + 1

    override fun getItemViewType(position: Int) = when {
        isActiveCategoryPosition(position) -> VIEW_TYPE_CATEGORY_ITEM
        isHeaderPosition(position) -> VIEW_TYPE_HEADER
        isArchivedCategoryPosition(position) -> VIEW_TYPE_CATEGORY_ITEM
        isFooterPosition(position) -> VIEW_TYPE_FOOTER

        else -> throw AssertionError("Should not happen. Invalid position: $position")
    }

    //
    // Internal API
    //

    internal fun setCategories(
        activeCategories: List<Category>,
        archivedCategories: List<Category>
    ) {
        this.activeCategories = activeCategories
        this.archivedCategories = archivedCategories

        notifyDataSetChanged()
    }

    internal fun setSelectedCategory(selectedCategory: Category?) {
        this.selectedCategory = selectedCategory

        notifyDataSetChanged()
    }

    //
    // Private helpers
    //

    private fun isActiveCategoryPosition(position: Int) =
        position in activeCategories.indices

    private fun isHeaderPosition(position: Int) =
        archivedCategories.isNotEmpty() && position == activeCategories.size

    private fun isArchivedCategoryPosition(position: Int): Boolean {
        if (archivedCategories.isEmpty())
            return false

        val start = activeCategories.size + 1
        val end = start + archivedCategories.size

        return position in start until end
    }

    private fun isFooterPosition(position: Int): Boolean {
        if (archivedCategories.isEmpty())
            return position == activeCategories.size

        return position == activeCategories.size + 1 + archivedCategories.size
    }
}
