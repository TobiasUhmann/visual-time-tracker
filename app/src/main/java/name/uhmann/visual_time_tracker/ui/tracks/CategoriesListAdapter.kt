package name.uhmann.visual_time_tracker.ui.tracks

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.data.Category
import kotlinx.android.synthetic.main.category_item.view.*


class CategoriesListAdapter internal constructor(
    context: Context,
    private val categories: List<Category>,
    private val onClickListener: (Category) -> Any
) : RecyclerView.Adapter<CategoriesListAdapter.CategoryViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
        CategoryViewHolder(inflater.inflate(R.layout.category_item, parent, false))

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]

        holder.itemView.categoryItem_nameTextView.text = category.name
        holder.itemView.categoryItem_shortText.text = category.shortText
        holder.itemView.categoryItem_shortText.setBackgroundColor(category.color)

        holder.itemView.setOnClickListener { onClickListener(categories[position]) }
    }

    override fun getItemCount() = categories.size
}
