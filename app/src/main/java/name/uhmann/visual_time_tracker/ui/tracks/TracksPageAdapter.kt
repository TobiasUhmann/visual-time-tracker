package name.uhmann.visual_time_tracker.ui.tracks

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

internal const val ARG_POSITION = "position"

class TracksPageAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun createFragment(position: Int): Fragment {
        val fragment = TracksPageFragment()
        fragment.arguments = Bundle().apply {
            putInt(ARG_POSITION, position)
        }

        return fragment
    }

    override fun getItemCount() = Int.MAX_VALUE
}
