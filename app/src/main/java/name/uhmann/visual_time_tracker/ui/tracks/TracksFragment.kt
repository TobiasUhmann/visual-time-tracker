package name.uhmann.visual_time_tracker.ui.tracks

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import name.uhmann.visual_time_tracker.*
import name.uhmann.visual_time_tracker.data.Track
import name.uhmann.visual_time_tracker.ui.ViewMode
import kotlinx.android.synthetic.main.tracks_fragment.*
import kotlinx.android.synthetic.main.tracks_fragment.view.*
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.util.*


class TracksFragment : Fragment() {

    private lateinit var viewModel: TracksViewModel

    //
    // Overridden methods
    //

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tracks_fragment, container, false)
        setHasOptionsMenu(true)

        val viewPager = view.tracksFragment_viewPager
        viewPager.adapter = TracksPageAdapter(this)
        viewPager.offscreenPageLimit = 1
        viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                onSwitchPage(position)
            }
        })

        view.tracksFragment_addTrackFab.setOnClickListener { onAddTrack() }

        viewModel = ViewModelProvider(this).get(TracksViewModel::class.java)
        viewModel.pageIndex.observe(viewLifecycleOwner, Observer { onPageIndexChange(it) })
        viewModel.date.observe(viewLifecycleOwner, Observer { onDateChange(it) })
        viewModel.viewMode.observe(viewLifecycleOwner, Observer { onViewModeChange(it) })

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.tracks_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val viewMode = viewModel.viewMode.value

        val dayViewItem = menu.findItem(R.id.tracks_showDayViewItem)
        dayViewItem.isVisible = viewMode == ViewMode.MONTH

        val weekViewItem = menu.findItem(R.id.tracks_showWeekViewItem)
        weekViewItem.isVisible = viewMode == ViewMode.DAY

        val monthViewItem = menu.findItem(R.id.tracks_showMonthViewItem)
        monthViewItem.isVisible = viewMode == ViewMode.WEEK
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.tracks_showStatistics -> onShowStatistics()
            R.id.tracks_showDayViewItem -> onShowDayView()
            R.id.tracks_showWeekViewItem -> onShowWeekView()
            R.id.tracks_showMonthViewItem -> onShowMonthView()
            R.id.tracks_pickDateItem -> onPickDate()

            else -> return false
        }

        return true
    }

    //
    // UI listeners
    //

    private fun onSwitchPage(position: Int) {
        val pageDelta = position - viewModel.pageIndex.value!!

        viewModel.pageIndex.value = position

        val currentDate = viewModel.date.value!!
        viewModel.date.value = when (viewModel.viewMode.value!!) {
            ViewMode.DAY -> currentDate.plusDays(pageDelta.toLong())
            ViewMode.WEEK -> currentDate.plusWeeks(pageDelta.toLong())
            ViewMode.MONTH -> currentDate.plusMonths(pageDelta.toLong())
        }
    }

    private fun onShowStatistics() {
        val from = viewModel.date.value!!
        val until = when (viewModel.viewMode.value!!) {
            ViewMode.DAY -> from.plusDays(1)
            ViewMode.WEEK -> from.plusWeeks(1)
            ViewMode.MONTH -> from.plusMonths(1)
        }

        val action = TracksFragmentDirections.actionTracksFragmentToStatisticsFragment(from, until)
        findNavController().navigate(action)
    }

    private fun onShowDayView() {
        viewModel.viewMode.value = ViewMode.DAY
    }

    private fun onShowWeekView() {
        viewModel.date.value = viewModel.date.value!!.with(DayOfWeek.MONDAY)
        viewModel.viewMode.value = ViewMode.WEEK
    }

    private fun onShowMonthView() {
        viewModel.date.value = viewModel.date.value!!.withDayOfMonth(1)
        viewModel.viewMode.value = ViewMode.MONTH
    }

    private fun onPickDate() {
        val currentDate = viewModel.date.value!!
        DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val pickedDate = LocalDate.of(year, month + 1, day)

                val viewMode = viewModel.viewMode.value!!

                val newDate = when (viewMode) {
                    ViewMode.DAY -> pickedDate
                    ViewMode.WEEK -> pickedDate.with(DayOfWeek.MONDAY)
                    ViewMode.MONTH -> pickedDate.withDayOfMonth(1)
                }

                val pageDelta = when (viewMode) {
                    ViewMode.DAY -> ChronoUnit.DAYS.between(currentDate, newDate)
                    ViewMode.WEEK -> ChronoUnit.WEEKS.between(currentDate, newDate)
                    ViewMode.MONTH -> ChronoUnit.MONTHS.between(currentDate, newDate)
                }

                viewModel.pageIndex.value = viewModel.pageIndex.value!! + pageDelta.toInt()
                viewModel.date.value = newDate
            },
            currentDate.year,
            currentDate.monthValue - 1,
            currentDate.dayOfMonth
        ).show()
    }

    /**
     * Day View        -> Today          -> Add track at current time
     *                    else           -> Time dialog (default now)
     *
     * Week/Month View -> Contains today -> Date dialog (default today), Time dialog (default now)
     *                    else           -> Date dialog (default 1st day), Time dialog (default now)
     */
    private fun onAddTrack() {
        val viewMode = viewModel.viewMode.value!!
        val date = viewModel.date.value!!

        val today = LocalDate.now()
        val now = LocalTime.now()

        if (viewMode == ViewMode.DAY) {
            if (date == today) {
                val track = Track(LocalDateTime.now(), null)
                viewModel.insertTrack(track) { id -> viewModel.selectedTrack.postValue(id) }

            } else {
                showTimeDialog { hour, minute ->
                    val dateTime = LocalDateTime.of(date, now.withHour(hour).withMinute(minute))
                    val track = Track(dateTime, null)
                    viewModel.insertTrack(track) { id -> viewModel.selectedTrack.postValue(id) }
                }
            }

        } else {
            val until = when (viewMode) {
                ViewMode.DAY -> date.plusDays(1)
                ViewMode.WEEK -> date.plusWeeks(1)
                ViewMode.MONTH -> date.plusMonths(1)
            }

            val dialogDate = if (date <= today && today < until) today else date

            showDateDialog(dialogDate) { year, month, day ->
                showTimeDialog { hour, minute ->
                    val dateTime = LocalDateTime.of(year, month, day, hour, minute)
                    val track = Track(dateTime, null)
                    viewModel.insertTrack(track) { id -> viewModel.selectedTrack.postValue(id) }
                }
            }
        }
    }

    //
    // ViewModel listeners
    //

    private fun onPageIndexChange(pageIndex: Int) {
        tracksFragment_viewPager.currentItem = pageIndex
    }

    private fun onDateChange(date: LocalDate) {
        val viewMode = viewModel.viewMode.value!!

        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.title = getActionBarTitle(viewMode, date)
    }

    private fun onViewModeChange(viewMode: ViewMode) {
        val date = viewModel.date.value!!

        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.title = getActionBarTitle(viewMode, date)

        requireActivity().invalidateOptionsMenu()
    }

    //
    // Private helpers
    //

    private fun showDateDialog(date: LocalDate, callback: (Int, Int, Int) -> Unit) {
        DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { _, year, month, day ->
                callback(year, month + 1, day)
            },
            date.year,
            date.monthValue - 1,
            date.dayOfMonth
        ).apply { datePicker.firstDayOfWeek = Calendar.MONDAY }.show()
    }

    private fun showTimeDialog(callback: (Int, Int) -> Unit) {
        val now = LocalTime.now()
        TimePickerDialog(
            requireContext(),
            TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                callback(hour, minute)
            },
            now.hour,
            now.minute,
            false
        ).show()
    }
}
