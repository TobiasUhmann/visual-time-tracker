package name.uhmann.visual_time_tracker.ui.statistics

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import name.uhmann.visual_time_tracker.data.AppDatabase
import name.uhmann.visual_time_tracker.data.AppRepository
import java.time.LocalDate

enum class ChartMode {
    BAR_CHART,
    PIE_CHART
}

class StatisticsViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = AppRepository(AppDatabase.getDatabase(application).dao())

    val chartMode = MutableLiveData<ChartMode>(ChartMode.BAR_CHART)
    val showUntracked = MutableLiveData(false)

    var items: List<StatisticsItem>? = null
    var allItems: List<StatisticsItem>? = null


    //
    // Tracks with category
    //

    fun getTracks(from: LocalDate, until: LocalDate) =
        repository.getTracksWithCategoryPlusSurrounding(from, until)
}
