package name.uhmann.visual_time_tracker.ui.categories

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.data.Category
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import kotlinx.android.synthetic.main.categories_fragment.view.*
import kotlinx.android.synthetic.main.text_dialog.view.*


class CategoriesFragment : Fragment() {

    private lateinit var viewModel: CategoriesViewModel

    private lateinit var adapter: CategoriesListAdapter

    //
    // Overridden methods
    //

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.categories_fragment, container, false)

        setHasOptionsMenu(true)

        //
        // Init RecyclerView
        //

        adapter = CategoriesListAdapter(
            view.context,
            { category -> onCategoryClicked(category) },
            { category -> onCategoryNameClicked(category) },
            { category -> onCategoryColorClicked(category) },
            { category -> onCategoryShortTextClicked(category) }
        )

        view.categories_recyclerView.layoutManager = LinearLayoutManager(view.context)
        view.categories_recyclerView.adapter = adapter

        //
        // Init FAB
        //

        view.categories_addCategoryFab.setOnClickListener { onAddCategoryFabClicked() }

        //
        // Init ViewModel
        //

        viewModel = ViewModelProvider(this).get(CategoriesViewModel::class.java)

        viewModel.selectCategories()
            .observe(viewLifecycleOwner, Observer { onCategoriesChanged(it) })

        viewModel.selectedCategory
            .observe(viewLifecycleOwner, Observer { onSelectedCategoryChanged(it) })

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.categories_menu, menu)
    }

    /**
     * active category selected   -> Show archive item
     * archived category selected -> Show unarchive and delete items
     */
    override fun onPrepareOptionsMenu(menu: Menu) {
        val selectedCategory = viewModel.selectedCategory.value

        val activeCategorySelected =
            if (selectedCategory == null) false else !selectedCategory.archived
        val archivedCategorySelected = selectedCategory?.archived ?: false

        val archiveItem = menu.findItem(R.id.categories_archiveCategoryItem)
        archiveItem.isVisible = activeCategorySelected

        val unarchiveItem = menu.findItem(R.id.categories_unarchiveCategoryItem)
        unarchiveItem.isVisible = archivedCategorySelected

        val deleteItem = menu.findItem(R.id.categories_deleteCategoryItem)
        deleteItem.isVisible = archivedCategorySelected
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.categories_archiveCategoryItem -> onArchiveCategoryItemClicked()
            R.id.categories_unarchiveCategoryItem -> onUnarchiveCategoryItemClicked()
            R.id.categories_deleteCategoryItem -> onDeleteCategoryItemClicked()

            else -> return false
        }

        return true
    }

    //
    // UI listeners
    //

    /**
     * Archive selected category
     */
    private fun onArchiveCategoryItemClicked() {
        val selectedCategory = viewModel.selectedCategory.value!!
        val updatedCategory = selectedCategory.copy(archived = true)

        viewModel.updateCategory(updatedCategory)
        viewModel.selectedCategory.value = updatedCategory
    }

    /**
     * Unarchive selected category
     */
    private fun onUnarchiveCategoryItemClicked() {
        val selectedCategory = viewModel.selectedCategory.value!!
        val updatedCategory = selectedCategory.copy(archived = false)

        viewModel.updateCategory(updatedCategory)
        viewModel.selectedCategory.value = updatedCategory
    }

    /**
     * Show delete category dialog. If confirmed, delete selected category
     */
    private fun onDeleteCategoryItemClicked() {
        AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.categories_deleteCategoryDialog_title))
            .setPositiveButton(android.R.string.yes) { _, _ ->
                deleteSelectedCategory()
            }
            .setNegativeButton(android.R.string.no) { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }

    /**
     * No category selected            -> Select this category
     * This category already selected  -> Unselect this category
     * Other category already selected -> Select this category, unselect other category
     */
    private fun onCategoryClicked(category: Category) {
        val selectedCategory = viewModel.selectedCategory.value
        viewModel.selectedCategory.value = when {
            selectedCategory == null -> category
            selectedCategory.id == category.id -> null
            else -> category
        }
    }

    /**
     * Show text dialog with category name. If confirmed, save new name
     */
    private fun onCategoryNameClicked(category: Category) {
        val inflater = LayoutInflater.from(requireContext())
        val textDialogView = inflater.inflate(R.layout.text_dialog, null)
        textDialogView.textDialog_editText.setText(category.name)

        AlertDialog.Builder(context)
            .setTitle(getString(R.string.categories_categoryNameDialog_title))
            .setView(textDialogView)
            .setPositiveButton(getString(R.string.dialog_ok)) { _, _ ->
                val updatedCategory =
                    category.copy(name = textDialogView.textDialog_editText.text.toString())
                viewModel.updateCategory(updatedCategory)
            }
            .setNegativeButton(getString(R.string.dialog_cancel)) { _, _ -> }
            .show()
    }

    /**
     * Show color picker with category color. If confirmed, save new color
     */
    private fun onCategoryColorClicked(category: Category) {
        ColorPickerDialogBuilder
            .with(context)
            .setTitle(R.string.categories_categoryColorDialog_title)
            .initialColor(category.color)
            .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
            .density(12)
            .setPositiveButton(R.string.dialog_ok) { _, selectedColor, _ ->
                val updatedCategory = category.copy(color = selectedColor)
                viewModel.updateCategory(updatedCategory)
            }
            .setNegativeButton(R.string.dialog_cancel) { _, _ -> }
            .build()
            .show()
    }

    /**
     * Show text dialog with category short_text. If confirmed, save new short_text
     */
    private fun onCategoryShortTextClicked(category: Category) {
        val inflater = LayoutInflater.from(requireContext())
        val textDialogView = inflater.inflate(R.layout.text_dialog, null)
        textDialogView.textDialog_editText.setText(category.shortText)

        AlertDialog.Builder(context)
            .setTitle(getString(R.string.categories_categoryShortTextDialog_title))
            .setView(textDialogView)
            .setPositiveButton(getString(R.string.dialog_ok)) { _, _ ->
                val updatedCategory =
                    category.copy(shortText = textDialogView.textDialog_editText.text.toString())
                viewModel.updateCategory(updatedCategory)
            }
            .setNegativeButton(getString(R.string.dialog_cancel)) { _, _ -> }
            .setOnCancelListener {}
            .show()
    }

    /**
     * Add new category with default name and color
     */
    private fun onAddCategoryFabClicked() {
        val name = resources.getString(R.string.categories_newCategory_name)
        val color = resources.getColor(R.color.categories_newCategory_color, null)
        val category = Category(0, name, color, false, "")

        viewModel.insertCategory(category)
    }

    //
    // ViewModel listeners
    //

    /**
     * Update categories in RecyclerView
     */
    private fun onCategoriesChanged(categories: List<Category>) {
        val activeCategories = categories.filterNot { it.archived }
        val archivedCategories = categories.filter { it.archived }

        adapter.setCategories(activeCategories, archivedCategories)
    }

    /**
     * Update selected category in RecyclerView
     */
    private fun onSelectedCategoryChanged(selectedCategory: Category?) {
        adapter.setSelectedCategory(selectedCategory)
        requireActivity().invalidateOptionsMenu()
    }

    //
    // Private helpers
    //

    private fun deleteSelectedCategory() {
        val selectedCategory = viewModel.selectedCategory.value!!

        viewModel.deleteCategory(selectedCategory.id)
        viewModel.selectedCategory.value = null
    }
}
