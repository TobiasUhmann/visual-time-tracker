package name.uhmann.visual_time_tracker.ui.statistics

import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.data.Category
import name.uhmann.visual_time_tracker.data.TrackWithCategory
import name.uhmann.visual_time_tracker.dpToPixels
import name.uhmann.visual_time_tracker.ui.statistics.StatisticsListAdapter.ListItem
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.*
import kotlinx.android.synthetic.main.statistics_fragment.*
import kotlinx.android.synthetic.main.statistics_fragment.view.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class StatisticsFragment : Fragment() {

    private lateinit var viewModel: StatisticsViewModel
    private lateinit var adapter: StatisticsListAdapter

    private lateinit var fromDate: LocalDate
    private lateinit var untilDate: LocalDate

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.statistics_fragment, container, false)
        setHasOptionsMenu(true)

        fromDate = arguments?.getSerializable("from") as LocalDate
        untilDate = arguments?.getSerializable("until") as LocalDate

        val switch = view.statistics_showUntrackedSwitch
        switch.setOnCheckedChangeListener { _, checked -> onShowUntracked(checked) }

        initBarChart(view.statistics_barChart)
        initPieChart(view.statistics_pieChart)

        adapter = StatisticsListAdapter(view.context)

        val recyclerView = view.statistics_recyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = object : LinearLayoutManager(view.context) {
            override fun canScrollVertically() = false
        }

        viewModel = ViewModelProvider(this).get(StatisticsViewModel::class.java)
        viewModel.chartMode.observe(viewLifecycleOwner, Observer { onChartModeChanged(it) })
        viewModel.showUntracked.observe(viewLifecycleOwner, Observer { onShowUntrackedChanged(it) })
        viewModel.getTracks(fromDate, untilDate)
            .observe(viewLifecycleOwner, Observer { onTracksChanged(it) })

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.statistics_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val chartMode = viewModel.chartMode.value!!

        val showBarChartItem = menu.findItem(R.id.statistics_showBarChartItem)
        showBarChartItem.isVisible = chartMode != ChartMode.BAR_CHART

        val showPieChartItem = menu.findItem(R.id.statistics_showPieChartItem)
        showPieChartItem.isVisible = chartMode != ChartMode.PIE_CHART
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.statistics_showBarChartItem -> onShowBarChart()
            R.id.statistics_showPieChartItem -> onShowPieChart()

            else -> return false
        }

        return true
    }

    //
    // UI listeners
    //

    private fun onShowBarChart() {
        viewModel.chartMode.value = ChartMode.BAR_CHART
    }

    private fun onShowPieChart() {
        viewModel.chartMode.value = ChartMode.PIE_CHART
    }

    private fun onShowUntracked(checked: Boolean) {
        viewModel.showUntracked.value = checked
    }

    //
    // ViewModel listeners
    //

    private fun onChartModeChanged(chartMode: ChartMode) {
        statistics_barChart.visibility = if (chartMode == ChartMode.BAR_CHART) VISIBLE else GONE
        statistics_pieChart.visibility = if (chartMode == ChartMode.PIE_CHART) VISIBLE else GONE

        requireActivity().invalidateOptionsMenu()
    }

    private fun onShowUntrackedChanged(checked: Boolean) {
        setData(checked)
    }

    private fun onTracksChanged(tracks: List<TrackWithCategory>) {
        calcAndStoreItems(tracks)
        setData(viewModel.showUntracked.value!!)
    }

    //
    // Helper methods
    //

    private fun initBarChart(chart: BarChart) {
        chart.description.isEnabled = false
        chart.legend.isEnabled = false
        chart.setTouchEnabled(false)
        chart.animateY(1000)

        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.setDrawAxisLine(false)
        chart.xAxis.setDrawLabels(false)

        chart.axisLeft.setDrawGridLines(false)
        chart.axisLeft.setDrawLabels(false)
        chart.axisLeft.setDrawAxisLine(false)
        chart.axisLeft.axisMinimum = 0f

        chart.axisRight.setDrawAxisLine(false)
        chart.axisRight.axisMinimum = 0f
    }

    private fun initPieChart(chart: PieChart) {
        chart.description.isEnabled = false
        chart.legend.isEnabled = false
        chart.setTouchEnabled(false)
        chart.animateY(1000)
    }

    private fun setBarChartItems(items: List<StatisticsItem>) {
        val barEntries = items.mapIndexed { index, entry ->
            BarEntry(index.toFloat(), entry.minutes / 60f)
        }

        val colors = items.map { it.color }

        val dataSet = BarDataSet(barEntries, null)
        dataSet.colors = colors

        val chart = statistics_barChart

        chart.data = BarData(dataSet)
        chart.data.setDrawValues(false)

        val barHeight = dpToPixels(requireContext(), 40)
        val fixedHeight = dpToPixels(requireContext(), 40)
        chart.layoutParams.height = barEntries.size * barHeight + fixedHeight
        chart.layoutParams = chart.layoutParams // apply dynamic layout params
    }

    private fun setPieChartItems(items: List<StatisticsItem>) {
        val pieEntries = items.map { PieEntry(it.minutes.toFloat()) }
        val colors = items.map { it.color }

        val dataSet = PieDataSet(pieEntries, null)
        dataSet.colors = colors

        val chart = statistics_pieChart
        chart.data = PieData(dataSet)
        chart.data.setDrawValues(false)
        chart.invalidate()
    }

    private fun calcAndStoreItems(allTracks: List<TrackWithCategory>) {

        val from = fromDate.atStartOfDay()
        val until = untilDate.atStartOfDay()

        var untrackedSeconds = 0L
        val categorySeconds = mutableMapOf<Int, Long>()

        fun addSeconds(category: Category?, seconds: Long) {
            if (category == null) {
                untrackedSeconds += seconds
            } else {
                val oldSeconds = categorySeconds.getOrDefault(category.id, 0)
                categorySeconds[category.id] = oldSeconds + seconds
            }
        }

        val preTrack = when {
            allTracks.isEmpty() -> null
            allTracks[0].track.dateTime < from -> allTracks[0]
            else -> null
        }

        val postTrack = when {
            allTracks.isEmpty() -> null
            allTracks[allTracks.lastIndex].track.dateTime >= until -> allTracks[allTracks.lastIndex]
            else -> null
        }

        val tracks = allTracks.toMutableList()
        if (preTrack != null) tracks.removeAt(0)
        if (postTrack != null) tracks.removeAt(tracks.lastIndex)

        when {
            tracks.isEmpty() -> {
                val now = LocalDateTime.now()
                // not before from, not after original until
                val untilEarly =
                    if (postTrack == null)
                        minOf(maxOf(from, now), until)
                    else
                        until

                val seconds = ChronoUnit.SECONDS.between(from, untilEarly)

                if (preTrack == null)
                    untrackedSeconds = seconds
                else if (seconds > 0)
                    addSeconds(preTrack.category, seconds)
            }

            else -> {
                val firstTrack = tracks[0]

                val preSeconds = ChronoUnit.SECONDS.between(from, firstTrack.track.dateTime)
                if (preSeconds > 0)
                    addSeconds(preTrack?.category, preSeconds)

                var prevTrack = firstTrack
                for (i in 2 until allTracks.size) {
                    val currTrack = allTracks[i]

                    val prevTime = prevTrack.track.dateTime
                    val currTime = currTrack.track.dateTime
                    val seconds = ChronoUnit.SECONDS.between(prevTime, currTime)
                    addSeconds(prevTrack.category, seconds)

                    prevTrack = currTrack
                }

                val now = LocalDateTime.now()
                val lastTrackTime = tracks[tracks.lastIndex].track.dateTime
                // not before last track, not after original until
                val untilEarly =
                    if (postTrack == null)
                        minOf(maxOf(lastTrackTime, now), until)
                    else
                        until

                val lastTrack = allTracks[allTracks.size - 1]
                val postSeconds = ChronoUnit.SECONDS.between(lastTrack.track.dateTime, untilEarly)
                addSeconds(lastTrack.category, postSeconds)
            }
        }

        val categoryNames = hashMapOf<Int, String>()
        val categoryColors = hashMapOf<Int, Int>()

        for (track in allTracks) {
            if (track.category != null) {
                categoryNames[track.category.id] = track.category.name
                categoryColors[track.category.id] = track.category.color
            }
        }

        val items = categorySeconds.toList()
            .map { (categoryId, seconds) ->
                StatisticsItem(
                    categoryNames[categoryId]!!,
                    seconds.toInt() / 60,
                    categoryColors[categoryId]!!
                )
            }

        val untrackedName = resources.getString(R.string.statistics_untrackedName)
        val untrackedColor = resources.getColor(R.color.colorPrimaryLight)
        val untrackedMinutes = untrackedSeconds.toInt() / 60
        val untrackedItem = StatisticsItem(untrackedName, untrackedMinutes, untrackedColor)

        viewModel.items = items.sortedByDescending { it.minutes }
        viewModel.allItems = (items + untrackedItem).sortedByDescending { it.minutes }
    }

    private fun setData(checked: Boolean) {
        val items = viewModel.items
        val allItems = viewModel.allItems

        if (items != null && allItems != null) {
            if (checked)
                setData(allItems)
            else
                setData(items)
        }
    }

    private fun setData(items: List<StatisticsItem>) {
        setBarChartItems(items.reversed())
        setPieChartItems(items)

        var totalMinutes = items.sumBy { it.minutes }
        if (totalMinutes == 0) totalMinutes = 1

        val listItems = items.map {
            ListItem(
                it.color,
                it.name,
                it.minutes / 60,
                it.minutes % 60,
                it.minutes * 100 / totalMinutes
            )
        }

        adapter.setItems(listItems)

        statistics_barChart.invalidate()
        statistics_pieChart.invalidate()
        statistics_recyclerView.invalidate()
    }
}
