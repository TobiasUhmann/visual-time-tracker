package name.uhmann.visual_time_tracker.ui

enum class ViewMode {
    DAY,
    WEEK,
    MONTH
}
