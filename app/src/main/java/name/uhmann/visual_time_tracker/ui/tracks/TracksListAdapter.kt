package name.uhmann.visual_time_tracker.ui.tracks

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.THUMBNAIL_SIZE
import name.uhmann.visual_time_tracker.data.TrackWithCategoryAndPhotos
import name.uhmann.visual_time_tracker.dpToPixels
import com.google.android.flexbox.FlexboxLayout
import kotlinx.android.synthetic.main.track_header_item.view.*
import kotlinx.android.synthetic.main.track_item.view.*
import java.lang.AssertionError
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

const val VIEW_TYPE_TRACK_ITEM = 1
const val VIEW_TYPE_HEADER = 2
const val VIEW_TYPE_FOOTER = 3

class TracksListAdapter internal constructor(
    private val context: Context,
    private val onClickListener: (TrackWithCategoryAndPhotos) -> Any,
    private val onLongClickListener: (TrackWithCategoryAndPhotos) -> Any,
    private val onTimeClickListener: (TrackWithCategoryAndPhotos) -> Any,
    private val onCameraClickListener: (TrackWithCategoryAndPhotos) -> Any,
    private val onPhotoClickListener: (TrackWithCategoryAndPhotos) -> Any,
    private val onColorClickListener: (TrackWithCategoryAndPhotos) -> Any
) : RecyclerView.Adapter<ViewHolder>() {

    private var items: List<Any> = emptyList()
    private var selectedTrack = RecyclerView.NO_POSITION

    //
    // Inner classes
    //

    inner class TrackViewHolder(itemView: View) : ViewHolder(itemView) {
        private lateinit var track: TrackWithCategoryAndPhotos

        private val timeText = itemView.trackItem_timeTextView
        private val flexBox = itemView.trackItem_flexboxLayout
        private val cameraIcon = itemView.trackItem_cameraImageView
        private val photoIcon = itemView.trackItem_photoImageView
        private val categoryIndicator = itemView.trackItem_categoryIndicator
        private val border = itemView.trackItem_border

        init {
            itemView.setOnClickListener { onClickListener(track) }
            itemView.setOnLongClickListener { onLongClickListener(track); true }
            timeText.setOnClickListener { onTimeClickListener(track) }
            cameraIcon.setOnClickListener { onCameraClickListener(track) }
            photoIcon.setOnClickListener { onPhotoClickListener(track) }
            categoryIndicator.setOnClickListener { onColorClickListener(track) }
        }

        fun bind(track: TrackWithCategoryAndPhotos) {
            this.track = track

            flexBox.removeViews(0, flexBox.childCount - 2)

            for (photo in track.photos) {
                val thumb = ImageView(context)

                val size = dpToPixels(context, THUMBNAIL_SIZE)
                thumb.layoutParams = FlexboxLayout.LayoutParams(size, size)

                thumb.scaleType = ImageView.ScaleType.CENTER_CROP

                thumb.setImageBitmap(photo.thumbnail)
                thumb.setBackgroundResource(R.drawable.track_item_border)
                thumb.contentDescription =
                    context.resources.getString(R.string.trackItem_thumbnail_contentDescription)

                val uri = Uri.parse(photo.filename)
                thumb.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                        .setDataAndType(uri, "image/*")
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        .addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                    context.startActivity(intent)
                }

                flexBox.addView(thumb, 0)
            }

            val timeFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)
            timeText.text = timeFormatter.format(track.track.dateTime)

            val isSelected = (track.track.id == selectedTrack)
            border.visibility = if (isSelected) View.VISIBLE else View.GONE
            cameraIcon.visibility = if (isSelected) View.VISIBLE else View.GONE
            photoIcon.visibility = if (isSelected) View.VISIBLE else View.GONE

            if (track.category != null) {
                categoryIndicator.text = track.category.shortText
                categoryIndicator.setBackgroundColor(track.category.color)
            } else {
                val defaultColor = ContextCompat.getColor(context, R.color.colorPrimaryLight)
                categoryIndicator.setBackgroundColor(defaultColor)
            }
        }
    }

    inner class HeaderViewHolder(itemView: View) : ViewHolder(itemView) {
        private val headerText = itemView.headerItem_text

        fun bind(text: String) {
            headerText.text = text
        }
    }

    internal class FooterViewHolder(itemView: View) : ViewHolder(itemView)

    //
    // Overridden methods
    //

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)

        return when (viewType) {
            VIEW_TYPE_TRACK_ITEM ->
                TrackViewHolder(inflater.inflate(R.layout.track_item, parent, false))

            VIEW_TYPE_HEADER ->
                HeaderViewHolder(inflater.inflate(R.layout.track_header_item, parent, false))

            VIEW_TYPE_FOOTER ->
                FooterViewHolder(inflater.inflate(R.layout.footer_item, parent, false))

            else -> throw AssertionError("Should not happen. Invalid view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewType = getItemViewType(position)

        if (viewType == VIEW_TYPE_TRACK_ITEM) {
            val item = items[position]
            (holder as TrackViewHolder).bind(item as TrackWithCategoryAndPhotos)
        } else if (viewType == VIEW_TYPE_HEADER) {
            val item = items[position]
            (holder as HeaderViewHolder).bind(item as String)
        }
    }

    override fun getItemCount() = items.size + 1

    override fun getItemViewType(position: Int) = when {
        position == items.size -> VIEW_TYPE_FOOTER
        items[position] is TrackWithCategoryAndPhotos -> VIEW_TYPE_TRACK_ITEM
        items[position] is String -> VIEW_TYPE_HEADER

        else -> throw AssertionError("Should not happen. Invalid item type.")
    }

    //
    // Internal API
    //

    internal fun setItems(items: List<Any>) {
        for (item in items)
            if (!(item is String || item is TrackWithCategoryAndPhotos))
                throw AssertionError()

        this.items = items
        notifyDataSetChanged()
    }

    internal fun setSelectedTrack(trackId: Int) {
        this.selectedTrack = trackId
        notifyDataSetChanged()
    }
}
