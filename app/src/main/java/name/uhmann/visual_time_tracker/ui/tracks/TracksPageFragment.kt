package name.uhmann.visual_time_tracker.ui.tracks

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.select_category_dialog.view.*
import kotlinx.android.synthetic.main.tracks_page_fragment.view.*
import name.uhmann.visual_time_tracker.R
import name.uhmann.visual_time_tracker.THUMBNAIL_SIZE
import name.uhmann.visual_time_tracker.data.Category
import name.uhmann.visual_time_tracker.data.Photo
import name.uhmann.visual_time_tracker.data.Track
import name.uhmann.visual_time_tracker.data.TrackWithCategoryAndPhotos
import name.uhmann.visual_time_tracker.dpToPixels
import name.uhmann.visual_time_tracker.getLongDayString
import name.uhmann.visual_time_tracker.ui.ViewMode
import java.io.File
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.OffsetDateTime
import kotlin.properties.Delegates


const val REQUEST_TAKE_PHOTO = 1
const val REQUEST_LOAD_PHOTO = 2


class TracksPageFragment : Fragment() {

    private lateinit var viewModel: TracksViewModel

    // Set by LiveData observer
    // Necessary to get latest categories
    private var categories: List<Category> = emptyList()

    private var requestForTrack = -1
    private var requestForUri: Uri? = null

    private var liveTracks: LiveData<List<TrackWithCategoryAndPhotos>>? = null
    private lateinit var liveTracksObserver: Observer<List<TrackWithCategoryAndPhotos>>

    private lateinit var adapter: TracksListAdapter
    private var pageIndex by Delegates.notNull<Int>()

    //
    // Overridden methods
    //

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tracks_page_fragment, container, false)

        pageIndex = getPageIndex(arguments)

        //
        // Init RecyclerView
        //

        adapter = TracksListAdapter(
            view.context,
            { track -> onTrackClicked(track) },
            { track -> onTrackLongClicked(track) },
            { track -> onTimeClicked(track) },
            { track -> onCameraClicked(track) },
            { track -> onPhotoClicked(track) },
            { track -> onCategoryClicked(track) }
        )

        val recyclerView = view.tracksPageFragment_recyclerView
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = adapter

        //
        // Init ViewModel
        //

        viewModel = ViewModelProvider(requireParentFragment()).get(TracksViewModel::class.java)

        viewModel.viewMode.observe(viewLifecycleOwner, Observer { onViewModeChanged(it) })
        viewModel.selectedTrack.observe(viewLifecycleOwner, Observer { onSelectedTrackChanged(it) })
        viewModel.selectCategories()
            .observe(viewLifecycleOwner, Observer { onCategoriesChanged(it) })

        liveTracksObserver = Observer { onTracksChange(it) }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                REQUEST_TAKE_PHOTO -> onPhotoTaken()
                REQUEST_LOAD_PHOTO -> onPhotoLoaded(data)
            }
        }
    }

    //
    // Activity result listeners
    //

    private fun onPhotoTaken() {
        val trackId = requestForTrack
        val uri = requestForUri

        processImage(trackId, uri!!)
    }

    private fun onPhotoLoaded(data: Intent?) {
        val trackId = requestForTrack
        val uri = data!!.data!!

        val resolver = requireContext().contentResolver
        resolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)

        processImage(trackId, uri)
    }

    //
    // UI listeners
    //

    /**
     * Click selected track -> deselect it
     * Click other track -> Select that one
     */
    private fun onTrackClicked(track: TrackWithCategoryAndPhotos) {
        viewModel.selectedTrack.value =
            if (track.track.id == viewModel.selectedTrack.value)
                -1
            else
                track.track.id
    }

    /**
     * Show delete dialog
     */
    private fun onTrackLongClicked(track: TrackWithCategoryAndPhotos) {
        val context = requireContext()
        AlertDialog.Builder(context)
            .setTitle(context.getString(R.string.tracks_deleteTrackDialog_title))
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.deleteTrack(track.track.id)
            }
            .setNegativeButton(android.R.string.no) { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }

    /**
     * Show TimePickerDialog -> Update track time
     */
    private fun onTimeClicked(oldTrack: TrackWithCategoryAndPhotos) {
        val context = requireContext()
        TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                val updatedTrack = Track(
                    oldTrack.track.dateTime.withHour(hour).withMinute(minute),
                    oldTrack.track.categoryId,
                    oldTrack.track.id
                )

                viewModel.updateTrack(updatedTrack)
            },
            oldTrack.track.dateTime.hour,
            oldTrack.track.dateTime.minute,
            false
        ).show()
    }

    /**
     * Open camera
     */
    private fun onCameraClicked(track: TrackWithCategoryAndPhotos) {
        val context = requireContext()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (intent.resolveActivity(context.packageManager) != null) {
            val timestamp = LocalDateTime.now().toEpochSecond(OffsetDateTime.now().offset)
            val dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            val file = File.createTempFile("JPEG_$timestamp", ".jpg", dir)

            val authority = "name.uhmann.visual_time_tracker.file_provider"
            val uri = FileProvider.getUriForFile(context, authority, file)

            requestForTrack = track.track.id
            requestForUri = uri

            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)

            startActivityForResult(intent, REQUEST_TAKE_PHOTO)
        }
    }

    private fun onPhotoClicked(track: TrackWithCategoryAndPhotos) {
        val intent = Intent()
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        requestForTrack = track.track.id
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_LOAD_PHOTO)
    }

    /**
     * Open category dialog
     */
    private fun onCategoryClicked(track: TrackWithCategoryAndPhotos) {
        val context = requireContext()

        val title = TextView(context).also {
            it.text = getString(R.string.tracks_selectCategoryDialog_title)
            it.setPadding(
                dpToPixels(
                    context,
                    16
                )
            )
            it.setTextAppearance(android.R.style.TextAppearance_DialogWindowTitle)
        }

        val dialog = AlertDialog.Builder(context)
            .setCustomTitle(title)
            .create()

        val dialogView = layoutInflater.inflate(R.layout.select_category_dialog, null)
        dialog.setView(dialogView)

        val adapter = CategoriesListAdapter(dialogView.context, categories) { category ->
            val updatedTrack = track.track.copy(categoryId = category.id)
            viewModel.updateTrack(updatedTrack)
            dialog.dismiss()
        }

        val recyclerView = dialogView.selectCategoryDialog_recyclerView
        recyclerView.layoutManager = LinearLayoutManager(dialogView.context)
        recyclerView.adapter = adapter

        dialog.show()
    }

    //
    // ViewModel listeners
    //

    private fun onCategoriesChanged(categories: List<Category>) {
        this.categories = categories.filterNot { it.archived }
    }

    private fun onSelectedTrackChanged(it: Int) {
        adapter.setSelectedTrack(it)
    }

    private fun onViewModeChanged(viewMode: ViewMode) {
        val date = viewModel.date.value!!
        val pageDelta = pageIndex - viewModel.pageIndex.value!!
        liveTracks?.removeObserver(liveTracksObserver)
        when (viewMode) {
            ViewMode.DAY -> {
                val day = date.plusDays(pageDelta.toLong())
                val nextDay = day.plusDays(1)
                liveTracks = viewModel.getTracks(day, nextDay)
                liveTracks!!.observe(viewLifecycleOwner, liveTracksObserver)
            }

            ViewMode.WEEK -> {
                val week = date.plusWeeks(pageDelta.toLong())
                val nextWeek = week.plusWeeks(1)
                liveTracks = viewModel.getTracks(week, nextWeek)
                liveTracks!!.observe(viewLifecycleOwner, liveTracksObserver)
            }

            ViewMode.MONTH -> {
                val month = date.plusMonths(pageDelta.toLong())
                val nextMonth = month.plusMonths(1)
                liveTracks = viewModel.getTracks(month, nextMonth)
                liveTracks!!.observe(viewLifecycleOwner, liveTracksObserver)
            }
        }
    }

    private fun onTracksChange(tracks: List<TrackWithCategoryAndPhotos>) {
        val items =
            if (viewModel.viewMode.value == ViewMode.DAY)
                tracks
            else
                getItems(tracks)

        adapter.setItems(items)
    }

    //
    // Helper methods
    //

    /**
     * Get date passed as argument. Should always be passed. If not, print warning
     * and return today's date.
     */
    private fun getPageIndex(arguments: Bundle?): Int {
        return if (arguments == null || !arguments.containsKey(ARG_POSITION)) {
            val warning = "No position passed. Should not happen. Taking 0."
            Log.w("###", "onCreateView: $warning")

            0
        } else {
            arguments.getInt(ARG_POSITION)
        }
    }

    private fun getItems(tracks: List<TrackWithCategoryAndPhotos>): MutableList<Any> {
        val listItems = mutableListOf<Any>()
        var lastDate: LocalDate? = null

        for (track in tracks) {
            val trackDate = track.track.dateTime.toLocalDate()
            if (trackDate != lastDate) {
                val header = getLongDayString(trackDate)
                listItems.add(header)
                lastDate = trackDate
            }

            listItems.add(track)
        }

        return listItems
    }

    /**
     * Create thumbnail and store it together with photo
     */
    private fun processImage(trackId: Int, uri: Uri) {

        val bitmap = MediaStore.Images.Media.getBitmap(requireContext().contentResolver, uri)

        fun Bitmap.rotate(degrees: Float): Bitmap {
            val matrix = Matrix().apply { postRotate(degrees) }
            return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
        }

        val inputStream = requireContext().contentResolver.openInputStream(uri)
        val exifInterface = ExifInterface(inputStream)
        val orientation = exifInterface.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            ExifInterface.ORIENTATION_ROTATE_90 -> bitmap.rotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> bitmap.rotate(180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> bitmap.rotate(270f)
            else -> bitmap
        }

        val size = dpToPixels(
            requireContext(),
            THUMBNAIL_SIZE
        )
        val thumbnail = ThumbnailUtils.extractThumbnail(rotatedBitmap, size, size)

        viewModel.insertPhoto(Photo(trackId, uri.toString(), thumbnail))
    }
}
