package name.uhmann.visual_time_tracker.ui.tracks

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import name.uhmann.visual_time_tracker.data.*
import name.uhmann.visual_time_tracker.ui.ViewMode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate

class TracksViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = AppRepository(AppDatabase.getDatabase(application).dao())

    var pageIndex = MutableLiveData(1000)
    var date = MutableLiveData<LocalDate>(LocalDate.now())

    val viewMode = MutableLiveData<ViewMode>(ViewMode.DAY)

    val selectedTrack = MutableLiveData<Int>()

    //
    // Tracks
    //

    fun insertTrack(track: Track, onInsert: (id: Int) -> Unit) =
        viewModelScope.launch(Dispatchers.IO) {
            val id = repository.insertTrack(track)
            onInsert.invoke(id)
        }

    fun updateTrack(track: Track) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateTrack(track)
    }

    fun deleteTrack(trackId: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteTrack(trackId)
    }

    //
    // Categories
    //

    fun selectCategories() = repository.selectCategories()

    //
    // Photos
    //

    fun insertPhoto(photo: Photo) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertPhoto(photo)
    }

    //
    // Tracks with category and photos
    //

    fun getTracks(from: LocalDate, until: LocalDate) =
        repository.getTracksWithCategoryAndPhotos(from, until)
}
