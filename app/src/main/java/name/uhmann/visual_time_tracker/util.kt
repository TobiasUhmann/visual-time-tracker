package name.uhmann.visual_time_tracker

import android.content.Context
import name.uhmann.visual_time_tracker.ui.ViewMode
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.WeekFields
import java.util.*

const val THUMBNAIL_SIZE = 48

/**
 * https://stackoverflow.com/a/5255256/4540114
 */
fun dpToPixels(context: Context, dp: Int): Int =
    (dp * context.resources.displayMetrics.density + 0.5).toInt()

//
// Date formatting
//

fun getShortDayString(date: LocalDate): String = when {
    date == LocalDate.now() -> "Today"
    date == LocalDate.now().minusDays(1) -> "Yesterday"
    date == LocalDate.now().plusDays(1) -> "Tomorrow"

    date.year == LocalDate.now().year ->
        DateTimeFormatter.ofPattern("EEE, MMM d").format(date)

    else -> DateTimeFormatter.ofPattern("EEE, MMM d, yyyy").format(date)
}

fun getLongDayString(date: LocalDate): String = when {
    date == LocalDate.now() -> "Today"
    date == LocalDate.now().minusDays(1) -> "Yesterday"
    date == LocalDate.now().plusDays(1) -> "Tomorrow"

    date.year == LocalDate.now().year ->
        DateTimeFormatter.ofPattern("EEEE, MMMM d").format(date)

    else -> DateTimeFormatter.ofPattern("EEEE, MMMM d, yyyy").format(date)
}

fun getWeekString(date: LocalDate): String {
    val weekFields: WeekFields = WeekFields.of(Locale.getDefault())
    val weekNumber = date[weekFields.weekOfWeekBasedYear()]

    return if (date.year == LocalDate.now().year)
        "Week $weekNumber"
    else
        "Week $weekNumber, ${date.year}"
}

fun getMonthString(date: LocalDate): String =
    if (date.year == LocalDate.now().year)
        DateTimeFormatter.ofPattern("MMMM").format(date)
    else
        DateTimeFormatter.ofPattern("MMMM, yyyy").format(date)

//
// Misc
//

fun getActionBarTitle(viewMode: ViewMode, date: LocalDate) = when (viewMode) {
    ViewMode.DAY -> getShortDayString(date)
    ViewMode.WEEK -> getWeekString(date)
    ViewMode.MONTH -> getMonthString(date)
}
