package name.uhmann.visual_time_tracker.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [Track::class, Photo::class, Category::class],
    version = 4,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun dao(): Dao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "app_database"
                )
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4)
                    .build()

                INSTANCE = instance
                return instance
            }
        }

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val sql = """
                ALTER TABLE categories
                ADD COLUMN archived INTEGER NOT NULL DEFAULT 0
            """

                database.execSQL(sql)
            }
        }

        private val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {

                val renameOldTable = "ALTER TABLE tracks RENAME TO old_tracks"

                val createNewTable = """
                    CREATE TABLE tracks (
                        track_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        
                        category_ref INTEGER,
                        
                        date_time INTEGER NOT NULL,
                        
                        FOREIGN KEY (category_ref) REFERENCES categories(category_id)
                            ON UPDATE CASCADE
                            ON DELETE SET NULL
                    )
                """

                val createIndex = "CREATE INDEX index_tracks_category_ref ON tracks(category_ref)"

                val copyOldToNew = """
                    INSERT INTO tracks (track_id, category_ref, date_time)
                    SELECT track_id, category_id_ref, dateTime
                    FROM old_tracks
                """

                val deleteOldTable = "DROP TABLE old_tracks"

                database.execSQL(renameOldTable)
                database.execSQL(createNewTable)
                database.execSQL(createIndex)
                database.execSQL(copyOldToNew)
                database.execSQL(deleteOldTable)
            }
        }

        /**
         * Add short_text column to categories table
         */
        private val MIGRATION_3_4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val sql = """
                ALTER TABLE categories
                ADD COLUMN short_text TEXT NOT NULL DEFAULT ""
            """

                database.execSQL(sql)
            }
        }
    }
}
