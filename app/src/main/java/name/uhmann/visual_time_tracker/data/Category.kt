package name.uhmann.visual_time_tracker.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categories")
data class Category(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "category_id")
    val id: Int = 0,

    val name: String,
    val color: Int,
    val archived: Boolean,

    @ColumnInfo(name = "short_text")
    val shortText: String
)
