package name.uhmann.visual_time_tracker.data

import androidx.room.Embedded
import androidx.room.Relation

data class TrackWithCategory(
    @Embedded
    val track: Track,

    @Relation(parentColumn = "category_ref", entityColumn = "category_id")
    val category: Category?
)
