package name.uhmann.visual_time_tracker.data

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao
import java.time.LocalDate

@Dao
interface Dao {

    //
    // Tracks
    //

    @Insert
    suspend fun addTrack(track: Track): Long

    @Update
    suspend fun updateTrack(track: Track)

    @Query("DELETE FROM tracks WHERE track_id = :trackId")
    suspend fun deleteTrack(trackId: Int)

    //
    // Categories
    //

    @Insert
    suspend fun insertCategory(category: Category)

    @Query("SELECT category_id, name, color, archived, short_text FROM categories")
    fun selectCategories(): LiveData<List<Category>>

    @Update
    suspend fun updateCategory(category: Category)

    @Query("DELETE FROM categories WHERE category_id = :categoryId")
    suspend fun deleteCategory(categoryId: Int)

    //
    // Photos
    //

    @Insert
    suspend fun addPhoto(photo: Photo)

    //
    // Tracks + further data
    //

    /**
     * Get the tracks between :from (inclusive) and :until (exclusive).
     * Also add the tracks before and after the selection if they
     * exist. The result is sorted by date time.
     */
    @Transaction
    @Query(
        """
        SELECT * 
        FROM (
            SELECT track_id, category_ref, date_time
            FROM tracks
            WHERE date_time <= :from
            ORDER BY date_time DESC
            LIMIT 1
        ) 
        UNION
        SELECT * 
        FROM (
            SELECT track_id, category_ref, date_time
            FROM tracks
            WHERE :from <= date_time AND date_time < :until
        )
        UNION
        SELECT *
        FROM (
            SELECT track_id, category_ref, date_time
            FROM tracks
            WHERE date_time >= :until
            ORDER BY date_time ASC
            LIMIT 1
        )
        ORDER BY date_time
        """
    )
    fun getTracksWithCategoryPlusSurrounding(
        from: LocalDate,
        until: LocalDate
    ): LiveData<List<TrackWithCategory>>

    @Transaction
    @Query(
        """
        SELECT track_id, category_ref, date_time
        FROM tracks
        WHERE date_time BETWEEN :from AND :until - 1
        ORDER BY date_time
        """
    )
    fun getTracksWithCategoryAndPhotos(
        from: LocalDate,
        until: LocalDate
    ): LiveData<List<TrackWithCategoryAndPhotos>>
}
