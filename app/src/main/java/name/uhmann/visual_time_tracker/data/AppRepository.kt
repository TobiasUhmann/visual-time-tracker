package name.uhmann.visual_time_tracker.data

import java.time.LocalDate

class AppRepository(private val dao: Dao) {

    //
    // Tracks
    //

    suspend fun insertTrack(track: Track) =
        dao.addTrack(track).toInt()

    suspend fun updateTrack(track: Track) {
        dao.updateTrack(track)
    }

    suspend fun deleteTrack(trackId: Int) {
        dao.deleteTrack(trackId)
    }

    //
    // Categories
    //

    suspend fun insertCategory(category: Category) {
        dao.insertCategory(category)
    }

    fun selectCategories() = dao.selectCategories()

    suspend fun updateCategory(category: Category) {
        dao.updateCategory(category)
    }

    suspend fun deleteCategory(categoryId: Int) {
        dao.deleteCategory(categoryId)
    }

    //
    // Photos
    //

    suspend fun insertPhoto(photo: Photo) {
        dao.addPhoto(photo)
    }

    //
    // Tracks + further data
    //

    fun getTracksWithCategoryPlusSurrounding(from: LocalDate, until: LocalDate) =
        dao.getTracksWithCategoryPlusSurrounding(from, until)

    fun getTracksWithCategoryAndPhotos(from: LocalDate, until: LocalDate) =
        dao.getTracksWithCategoryAndPhotos(from, until)
}
