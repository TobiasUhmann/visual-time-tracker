package name.uhmann.visual_time_tracker.data

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import androidx.room.ForeignKey.SET_NULL
import java.time.LocalDateTime

@Entity(
    tableName = "tracks",
    foreignKeys = [
        ForeignKey(
            entity = Category::class,
            parentColumns = ["category_id"],
            childColumns = ["category_ref"],
            onUpdate = CASCADE,
            onDelete = SET_NULL
        )
    ],
    indices = [
        Index(value = ["category_ref"])
    ]
)
data class Track(
    @ColumnInfo(name = "date_time")
    val dateTime: LocalDateTime,

    @ColumnInfo(name = "category_ref")
    val categoryId: Int?,

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "track_id")
    var id: Int = 0
)
