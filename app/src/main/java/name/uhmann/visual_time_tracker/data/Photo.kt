package name.uhmann.visual_time_tracker.data

import android.graphics.Bitmap
import androidx.room.*
import androidx.room.ForeignKey.CASCADE

@Entity(
    tableName = "photos",
    foreignKeys = [
        ForeignKey(
            entity = Track::class,
            parentColumns = ["track_id"],
            childColumns = ["track_id_ref"],
            onDelete = CASCADE
        )
    ],
    indices = [
        Index(value = ["track_id_ref"])
    ]
)
data class Photo(
    @ColumnInfo(name = "track_id_ref")
    val trackId: Int,

    val filename: String,

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    val thumbnail: Bitmap
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "photo_id")
    var id: Int = 0
}
