package name.uhmann.visual_time_tracker.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.TypeConverter
import java.io.ByteArrayOutputStream
import java.time.*

class Converters {

    //
    // LocalDateTime <-> Long
    //

    @TypeConverter
    fun localDateTimeToLong(localDateTime: LocalDateTime): Long =
        localDateTime.toEpochSecond(OffsetDateTime.now().offset)

    @TypeConverter
    fun longToLocalDateTime(epochSecond: Long): LocalDateTime =
        LocalDateTime.ofInstant(Instant.ofEpochSecond(epochSecond), ZoneId.systemDefault())

    //
    // LocalDate -> Long
    //

    @TypeConverter
    fun localDateToLong(value: LocalDate): Long =
        value.atStartOfDay().toEpochSecond(OffsetDateTime.now().offset)

    //
    // Bitmap <-> ByteArray
    //

    @TypeConverter
    fun bitmapToByteArray(bitmap: Bitmap): ByteArray {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)

        return outputStream.toByteArray()
    }

    @TypeConverter
    fun byteArrayToBitmap(byteArray: ByteArray): Bitmap =
        BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
}
