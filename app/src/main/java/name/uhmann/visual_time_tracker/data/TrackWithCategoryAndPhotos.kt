package name.uhmann.visual_time_tracker.data

import androidx.room.Embedded
import androidx.room.Relation

data class TrackWithCategoryAndPhotos(
    @Embedded
    val track: Track,

    @Relation(parentColumn = "category_ref", entityColumn = "category_id")
    val category: Category?,

    @Relation(parentColumn = "track_id", entityColumn = "track_id_ref")
    val photos: List<Photo>
)
